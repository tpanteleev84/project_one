﻿using HelloWorld.code.util;

namespace HelloWorld.code {
    class EverythingWriter {
        private uint iterations = 1000;
        public string Result { get; set; }

        public EverythingWriter(uint iterations) {
            this.iterations = iterations;
        }

        public void WriteEverthing(out string something) {
            something = "";

            CustomConsole.WritePlainText("Hello world");
            for (int i = 1; i <= iterations; i++) {
                Result += i;
                if (i % 100 == 0) {
                    CustomConsole.WritePlainText("Fucking progress...");
                    if (something.Length > 0)
                        something += " ";
                    something += i.ToString();
                }
            }
        }
    }
}
