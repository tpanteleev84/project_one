﻿using HelloWorld.code.common.form;
using HelloWorld.code.common.module.core.transport.socket;
using HelloWorld.code.common.module.model;
using HelloWorld.code.pattern.command;
using HelloWorld.code.util;
using HelloWorld.test;
using System;

namespace HelloWorld.code {
    internal class Game {
        private GameModel gameModel = null;
        private SocketClient socketClient = null;
        private Client client = null;

        public void InitGame() {
            socketClient = new SocketClient();
            gameModel = new GameModel(socketClient);
            //TODO: Bind ShowMainMenu on the global timer, not call its from ShowMainMenu
            socketClient.Connect();
            ShowMainMenu(true);
        }

        private void ShowMainMenu(bool isFirstCall) {
            if (!isFirstCall) {
                CustomConsole.WritePlainText("Press Enter");
                Console.ReadLine();
            }

            string command = ReadConsoleCommand();

            if (gameModel.SessionModel.Uid > 0) {
                switch (command) {
                    case CommandType.REGISTER_NEW_USER:
                    case CommandType.LOGIN:
                        CustomConsole.WriteWarning("Player already login to the game");
                        ShowMainMenu(false);
                        return;
                    default:
                        break;
                }
            }

            switch (command) {
                case CommandType.REGISTER_NEW_USER:
                    RegistrationForm registation = new RegistrationForm(gameModel);
                    registation.RegisterCompleted += OnRegisterCompletedHandler;
                    registation.Show();
                    break;
                case CommandType.LOGIN:
                    LoginForm login = new LoginForm(gameModel);
                    login.LoginCompleted += OnLoginCompletedHandler;
                    login.Show();
                    break;
                case CommandType.TEST:
                    EverythingWriter everythingWriter = new EverythingWriter((uint)new Random().Next(500, 5000));
                    string everything = "";
                    everythingWriter.WriteEverthing(out everything);
                    CustomConsole.WritePlainText("Do everything " + everything);
                    ShowMainMenu(false);
                    break;
                case CommandType.START_GAME:
                    client = new Client(gameModel);
                    client.Start();
                    break;
                case CommandType.DESERIALIZE:
                    XMLSerializeTester xmLSerializeTester = new XMLSerializeTester("../../test/test.xml", "units", new TestUnitList());
                    xmLSerializeTester.Serialize();
                    ShowMainMenu(false);
                    break;
                default:
                    CustomConsole.WritePlainText("Invalid command");
                    ShowMainMenu(false);
                    break;
            }
        }

        private string ReadConsoleCommand() {
            Console.Clear();
            CustomConsole.WritePlainText("Main menu:");
            string command = Console.ReadLine();
            Console.Clear();

            return command;
        }

        private void OnRegisterCompletedHandler(RegistrationForm sender, EventArgs args) {
            sender.RegisterCompleted -= OnRegisterCompletedHandler;
            ShowMainMenu(false);
        }

        private void OnLoginCompletedHandler(LoginForm sender, EventArgs args) {
            sender.LoginCompleted -= OnLoginCompletedHandler;
            ShowMainMenu(false);
        }

    }
}
