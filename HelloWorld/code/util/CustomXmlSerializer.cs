﻿using System.Xml;
using System.Xml.Serialization;

namespace HelloWorld.code.util {
    internal class CustomXmlSerializer {
        private object serializableObjectInstance = null;
        private string rootElementName = "";
        private string uri = "";

        public CustomXmlSerializer(string uri, string rootElementName, object serializableObjectInstance) {
            this.uri = uri;
            this.rootElementName = rootElementName;
            this.serializableObjectInstance = serializableObjectInstance;
        }

        public object Serialize() {
            return null;
        }

        public object Deserialize() {
            XmlSerializer serializer = new XmlSerializer(serializableObjectInstance.GetType(), new XmlRootAttribute(rootElementName));
            using (XmlReader xmlReader = XmlReader.Create(uri)) {
                return serializer.Deserialize(xmlReader);
            }
        }
    }
}
