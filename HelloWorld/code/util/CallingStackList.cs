﻿using HelloWorld.test;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace HelloWorld.code.util {
    class CallingStackList {
        private static CallingStackList instance = null;

        public static CallingStackList Instance {
            get {
                if (instance == null)
                    instance = new CallingStackList();

                return instance;
            }
        }

        public CallingStackList() {
            if (instance != null)
                throw new Exception("CallingStackList singleton object already have a instance");
        }

        //Hard, but okay for education
        public void PrintMessageWithTabs(string message, string methodName) {
            int tabCount = CurrentStackMethodNames.Where(stackMethodName => stackMethodName.Contains(methodName)).Count();
            message = message.Insert(0, new string('.', TestConstants.SPACE_NUMBER_IN_CONSOL_TAB * tabCount));
            CustomConsole.WritePlainText(message);
        }

        private LinkedList<string> CurrentStackMethodNames {
            get {
                LinkedList<string> currentStack = new LinkedList<string>();
                StackTrace stackTrace = new StackTrace(true);

                for (int i = 0; i < stackTrace.FrameCount; i++) {
                    StackFrame stackFrame = stackTrace.GetFrame(i);
                    string methodName = stackTrace.GetFrame(i).GetMethod().Name;
                    currentStack.AddLast(methodName);
                    //CustomConsole.WritePlainText("StackMethodName: " + methodName);
                }

                return currentStack;
            }
        }
    }
}
