﻿using HelloWorld.code;

namespace HelloWorld {
    class Program {
        static void Main(string[] args) {
            Game game = new Game();
            game.InitGame();
        }
    }
}
