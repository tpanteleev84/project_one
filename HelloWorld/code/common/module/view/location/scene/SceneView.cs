﻿using HelloWorld.code.common.library.location;
using HelloWorld.code.common.module.model.location;
using HelloWorld.code.util;
using HelloWorld.contentserver.generator.mob;

namespace HelloWorld.code.common.module.view.location.scene {
    internal class SceneView {
        private SceneData _sceneData = null;
        private SceneModel _sceneModel = null;
        private IUnitFactory _unitFactory = null;
        private string _terrainDescription = null;

        public SceneView() {
        }

        public SceneData SceneData {
            private get => _sceneData;
            set {
                if (_sceneData != null) {
                    CustomConsole.WriteWarning("SceneView: SceneData with id = " + _sceneData.Id + " already exist");
                } else {
                    _sceneData = value;
                }
            }
        }

        public SceneModel SceneModel {
            private get => _sceneModel;
            set {
                if (_sceneModel != null) {
                    CustomConsole.WriteWarning("SceneView: SceneModel with id = " + _sceneModel.Id + " already exist");
                } else {
                    _sceneModel = value;
                }
            }
        }

        public IUnitFactory UnitFactory {
            private get => _unitFactory;
            set {
                if (_unitFactory != null) {
                    CustomConsole.WriteWarning("SceneView: UnitFactory already exist");
                } else {
                    _unitFactory = value;
                }
            }
        }

        public string TerrainDescription {
            get => _terrainDescription;
            set {
                if (_terrainDescription != null) {
                    CustomConsole.WriteWarning("SceneView: TerrainDescription already exist");
                } else {
                    _terrainDescription = value;
                }
            }
        }

        public void init() {
            CreateUnits();
            CreateNPCs();
            CreateItems();
            CreateView();
        }

        protected void CreateUnits() {
        }

        protected void CreateNPCs() {
        }

        protected void CreateItems() {

        }

        protected void CreateView() {
            //Factory method
        }

        public void Clean() {
            _sceneData = null;
            _sceneModel = null;
            _unitFactory = null;
            _terrainDescription = null;
        }
    }
}
