﻿using HelloWorld.code.common.library.location;
using HelloWorld.code.common.module.model.location;
using HelloWorld.code.common.module.view.location.scene;
using HelloWorld.code.util;
using HelloWorld.contentserver.generator.location;

namespace HelloWorld.code.common.module.core {
    //TODO: Location View responsible for weather changing, common nature description, road description
    internal class LocationView {
        private SceneView[,] scenes = null;
        private LocationData _locationData = null;
        public LocationData LocationData {
            get => _locationData;
            set {
                if (_locationData != null) {
                    CustomConsole.WriteWarning("LocationView: LocationData with id = " + _locationData.Id + " already exist");
                } else {
                    _locationData = value;
                }
            }
        }
        private LocationModel _locationModel = null;
        public LocationModel LocationModel {
            private get => _locationModel;
            set {
                if (_locationData != null) {
                    CustomConsole.WriteWarning("LocationView: LocationData with id = " + _locationModel.Id + " already exist");
                } else {

                }
            }
        }

        public LocationView() {
        }

        public void Init() {
            scenes = new SceneView[10, 10];
            SceneView currentSceneView = LocationViewBuilder.Instance.CreateSceneView(LocationModel, LocationModel.CurrentScene);

            currentSceneView.init();
            scenes[0, 0] = currentSceneView;
        }

        public void ShowScene() {
            CustomConsole.WritePlainText(scenes[0, 0].TerrainDescription);
        }

        public void Clean() {
            scenes = null;
            _locationData = null;
        }
    }
}
