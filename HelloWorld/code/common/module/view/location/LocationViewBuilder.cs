﻿using HelloWorld.code.common.library;
using HelloWorld.code.common.library.location;
using HelloWorld.code.common.module.core;
using HelloWorld.code.common.module.model.location;
using HelloWorld.code.common.module.model.unit;
using HelloWorld.code.common.module.view.location.scene;
using HelloWorld.code.module.core.units;
using HelloWorld.code.util;
using HelloWorld.contentserver.generation.location.terrain;
using HelloWorld.contentserver.generator.mob;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HelloWorld.contentserver.generator.location {
    //pattern singletone
    internal class LocationViewBuilder {
        private static LocationViewBuilder instance = null;

        private LocationView[] locationViewObjectPool = null;
        private SceneView[] sceneViewObjectPool = null;

        private TerrainBuilderDirector terrainBuilderDirector = null;
        private Dictionary<LocationType, IUnitFactory> unitFactoryByLocation = null;

        public static LocationViewBuilder Instance {
            get {
                if (instance == null)
                    instance = new LocationViewBuilder();

                return instance;
            }
        }

        public LocationViewBuilder() {
            if (instance != null)
                throw new Exception("LocationObjectsBuilder singleton object already have a instance");

            locationViewObjectPool = new LocationView[3];
            sceneViewObjectPool = new SceneView[10];

            unitFactoryByLocation = new Dictionary<LocationType, IUnitFactory>();
            unitFactoryByLocation.Add(LocationType.PLANE, new PlaneUnitFactory());
            unitFactoryByLocation.Add(LocationType.FOREST, new ForestUnitFactory());
            unitFactoryByLocation.Add(LocationType.ISLAND, new IslandUnitFactory());
            unitFactoryByLocation.Add(LocationType.ROCK, new RockUnitFactory());
        }

        public LocationView CreateLocationView(LocationModel locationModel) {
            LocationView newLocationView = null;

            if (locationViewObjectPool.Length > 0) {
                newLocationView = locationViewObjectPool.First();
                locationViewObjectPool[0] = null;
            } else {
                newLocationView = new LocationView();
            }

            newLocationView.LocationData = GameLibrary.Instance.Locations.GetLocationById(locationModel.Id);
            newLocationView.LocationModel = locationModel;


            //TODO: Not use
            switch (newLocationView.LocationData.Type) {
                case LocationType.PLANE:
                    break;
                case LocationType.FOREST:
                    break;
                case LocationType.ISLAND:
                    break;
                case LocationType.ROCK:
                    break;
                default:
                    CustomConsole.WriteError("LocationBuilder: Location type " + newLocationView.LocationData.Type.ToString() + " have not a realization");
                    break;
            }

            return newLocationView;
        }

        public SceneView CreateSceneView(LocationModel locationModel, SceneModel sceneModel) {
            SceneView sceneView = null;

            if (sceneViewObjectPool.Length > 0) {
                sceneView = sceneViewObjectPool.First();
                sceneViewObjectPool[0] = null;
            } else {
                sceneView = new SceneView();
            }
            
            sceneView.SceneData = GameLibrary.Instance.Locations.getSceneDataById(locationModel.Id, sceneModel.Id);
            sceneView.SceneModel = sceneModel;

            return sceneView;
        }

        public string MakeTerrain(LocationType locationType, SceneData sceneData) {
            return terrainBuilderDirector.MakeTerrain(sceneData);
        }

        //TODO: use unit factory
        public IUnit MakeUnit(string data) {
            IUnit newUnit = null;
            UnitType type = UnitType.NPC;

            switch (type) {
                case UnitType.NPC:
                    newUnit = new NPCUnitModel(data);
                    break;
                case UnitType.ENEMY_LAND:
                    newUnit = new EnemyUnitModel(data);
                    break;
                case UnitType.ENEMY_WATERFOWL:
                    newUnit = new EnemyUnitModel(data);
                    break;
                case UnitType.ENEMY_FLYING:
                    newUnit = new EnemyUnitModel(data);
                    break;
                case UnitType.ENEMY_UNDERGROUND:
                    newUnit = new EnemyUnitModel(data);
                    break;
                default:
                    CustomConsole.WritePlainText("LocationBuilder: Unit type  " + type.ToString() + " have not a realization");
                    break;
            }

            return newUnit;
        }
    }
}
