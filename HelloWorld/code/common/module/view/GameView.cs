﻿using HelloWorld.code.common.library;
using HelloWorld.code.common.library.location;
using HelloWorld.code.common.module.controller;
using HelloWorld.code.common.module.core;
using HelloWorld.code.common.module.model;
using HelloWorld.code.common.view.gui;
using HelloWorld.contentserver.generator.location;

namespace HelloWorld.code.common.module.view {
    class GameView {
        private GameModel gameModel = null;
        private GameController gameController = null;

        private LocationView locationView = null;
        private VirtualGUI virtualGUI = null;

        public GameView(GameModel gameModel, GameController gameController) {
            this.gameModel = gameModel;
            this.gameController = gameController;

            this.virtualGUI = new VirtualGUI(gameModel, gameController);
        }

        public void InitLocation() {
            locationView = LocationViewBuilder.Instance.CreateLocationView(gameModel.LocationModel);
            locationView.Init();
            locationView.ShowScene();
        }

        public void RunGUI() {
            virtualGUI.Run();
        }
    }
}
