﻿using HelloWorld.code.common.module.controller;
using HelloWorld.code.common.module.controller.command;
using HelloWorld.code.common.module.model;
using HelloWorld.code.common.module.model.location;
using HelloWorld.code.util;
using System;
using System.Collections.Generic;
using System.Threading;

namespace HelloWorld.code.common.view.gui {
    class VirtualGUI {
        private Thread thread = null;
        private GameModel gameModel = null;
        private GameController gameController = null;
        public VirtualGUI(GameModel gameModel, GameController gameController) {
            this.gameModel = gameModel;
            this.gameController = gameController;
            thread = new Thread(new ThreadStart(ConsoleCommandHandler));
        }
        public void Run() {
            this.InitCurrentScene();

            if (!thread.IsAlive)
                thread.Start();
        }

        private void InitCurrentScene() {
            //TODO: Teach the console write blocks
            CustomConsole.WritePlainText("...");
            this.OutputCurrentSceneActionList();
            CustomConsole.WritePlainText("...");
        }

        private void OutputCurrentSceneActionList() {
            List<string> actionList = new List<string>();
            if (gameModel.LocationModel.CurrentScene.HasEnemy()) {
                actionList.Add("* Attack enemy -> '" + VirtualInterfaceCommandType.ATTACK_THE_ENEMY + "'");
            }
            if (gameModel.LocationModel.CurrentScene.HasNPC()) {
                actionList.Add("* Speak with NPC -> '" + VirtualInterfaceCommandType.SPEAK_WITH_NPC + "'");
            }
            if (gameModel.LocationModel.CurrentScene.HasLoot()) {
                actionList.Add("* Search the corps -> '" + VirtualInterfaceCommandType.SEARCH_THE_CORPS + "'");
            }

            actionList.Add("* Go to the next scene -> '" + VirtualInterfaceCommandType.GO_TO_THE_NEXT_SCENE + "'");
        }

        //Separate thread for inteface with realtime command enter.
        private void ConsoleCommandHandler() {
            CustomConsole.WritePlainText("Enter the command command... So... Butter buttered");
            string[] commandPatternCommand = Console.ReadLine().Split(Char.Parse(" "));

            try {
                while (true) {
                    switch (commandPatternCommand[0]) {
                        case VirtualInterfaceCommandType.GO_TO_THE_NEXT_SCENE:
                            DirectionType direction = DirectionType.UNKNOWN;
                            Enum.TryParse<DirectionType>(commandPatternCommand[1], out direction);
                            gameController.GoToTheNextScene(direction);
                            break;
                        case VirtualInterfaceCommandType.SEARCH_THE_CORPS:
                            gameController.SearhTheCorps("data"); //Split command for params
                            break;
                        case VirtualInterfaceCommandType.SPEAK_WITH_NPC:
                            gameController.SpeakWithNPC("data"); //Split command for params
                            break;
                        case VirtualInterfaceCommandType.ATTACK_THE_ENEMY:
                            gameController.SpeakWithNPC("data"); //Split command for params
                            break;
                        case VirtualInterfaceCommandType.USE_ITEM:
                            gameController.UseItem("data");
                            break;
                        default:
                            CustomConsole.WritePlainText("Invalid command");
                            break;
                    }

                    commandPatternCommand = Console.ReadLine().Split(Char.Parse(" "));
                    Thread.Sleep(100);
                }
            } catch (ThreadAbortException abortException) {
                CustomConsole.WritePlainText((string)abortException.ExceptionState);
            }
        }
    }
}
