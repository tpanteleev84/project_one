﻿using System;

namespace HelloWorld.code.common.module.core.transport.socket {
    interface IServerCommand {
        event EventHandler CommandCompleted;
        string CommandType { get; }
        string CommandString { get; }
        void ParseResponse(string jsonResponse); 
    }
}
