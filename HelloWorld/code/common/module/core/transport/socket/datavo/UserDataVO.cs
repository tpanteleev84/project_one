﻿namespace HelloWorld.code.common.module.core.transport.socket.datavo {
    internal class UserDataVO : DataVOBase {
        public uint uid;
        public string name;
        public string email;
        public uint location_id;
        public uint scene_id;
    }
}
