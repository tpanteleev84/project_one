﻿namespace HelloWorld.code.common.module.core.transport.socket.datavo {
    internal class SessionDataVO : DataVOBase {
        public uint uid;
        public uint user_uid;
        public uint serverTime;
        public string token;
    }
}
