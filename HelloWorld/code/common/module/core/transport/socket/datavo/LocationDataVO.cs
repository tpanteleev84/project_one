﻿namespace HelloWorld.code.common.module.core.transport.socket.datavo {
    internal class LocationDataVO : DataVOBase {
        public uint id;
        public uint uid;
        public uint currentSceneId;
    }
}
