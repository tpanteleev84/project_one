﻿namespace HelloWorld.code.common.module.core.transport.socket.datavo {
    internal class SceneDataVO : DataVOBase {
        public uint id;
        public uint uid;
        public uint locationId;
        public uint locationUid;
        public string description;
    }
}
