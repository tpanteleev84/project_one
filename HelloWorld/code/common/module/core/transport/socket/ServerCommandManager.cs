﻿using HelloWorld.code.common.module.core.transport.socket.command;
using HelloWorld.code.common.module.model;
using HelloWorld.code.common.module.model.location;
using HelloWorld.contentserver;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HelloWorld.code.common.module.core.transport.socket {
    class ServerCommandManager {
        private Dictionary<string, Func<IServerCommand, bool>> commandHandlerMap = null;
        private GameModel gameModel = null;
        //TODO: use when fake server will be separate
        private SocketClient socketClient = null;
        private FakeContentSocketServer fakeContentSocketServer = null;
        private List<IServerCommand> commandsQueue = null;
        private IServerCommand executingCommand = null;

        public ServerCommandManager(GameModel gameModel, SocketClient socketClient) {
            this.gameModel = gameModel;
            this.socketClient = socketClient;
            this.fakeContentSocketServer = new FakeContentSocketServer(socketClient);

            commandsQueue = new List<IServerCommand>();

            commandHandlerMap = new Dictionary<string, Func<IServerCommand, bool>>();
            commandHandlerMap.Add(ServerCommandType.REGISTER_NEW_USER, OnNewUserRegisterHandler);
            commandHandlerMap.Add(ServerCommandType.LOGIN, OnLoginHandler);
            commandHandlerMap.Add(ServerCommandType.GET_SCENE, OnMakeSceneHandler);
            commandHandlerMap.Add(ServerCommandType.GET_LOCATION, OnMakeLocationHandler);
        }

        public void ExecuteCommand(IServerCommand command) {
            commandsQueue.Add(command);
            ExecuteNextCommand();
        }

        private void ExecuteNextCommand() {
            if (executingCommand != null)
                return;

            if (commandsQueue.Count == 0)
                return;

            executingCommand = commandsQueue.First();
            if (executingCommand == null)
                return;

            commandsQueue.RemoveAt(0);
            executingCommand.CommandCompleted += OnCommandCompleted;

            fakeContentSocketServer.ReceiveServerRequest(executingCommand);
        }

        private void OnCommandCompleted(object sender, EventArgs args) {
            executingCommand.CommandCompleted -= OnCommandCompleted;
            Func<IServerCommand, bool> commandResponseHandler = commandHandlerMap[executingCommand.CommandType];
            commandResponseHandler(executingCommand);

            executingCommand = null;
            ExecuteNextCommand();
        }

        private bool OnMakeLocationHandler(IServerCommand arg) {
            GetLocationServerCommand command = (GetLocationServerCommand)arg;
            LocationModel locationModel = gameModel.GetLocationById(command.Response.Location.id);
            gameModel.LocationModel.Update(command.Response.Location);
            return true;
        }

        private bool OnMakeSceneHandler(IServerCommand arg) {
            GetSceneServerCommand command = (GetSceneServerCommand)arg;
            LocationModel locationModel = gameModel.GetLocationById(command.Response.Scene.locationId);
            SceneModel sceneModel = locationModel.GetSceneById(command.Response.Scene.id);

            sceneModel.Update(command.Response.Scene);
            return true;
        }

        private bool OnLoginHandler(IServerCommand arg) {
            LoginServerCommand command = (LoginServerCommand)arg;
            gameModel.UserModel.Update(command.Response.User);
            gameModel.SessionModel.Update(command.Response.Session);
            return true;
        }

        private bool OnNewUserRegisterHandler(IServerCommand arg) {
            RegisterNewUserServerCommand command = (RegisterNewUserServerCommand)arg;
            gameModel.UserModel.Update(command.Response.User);
            return true;
        }
    }
}
