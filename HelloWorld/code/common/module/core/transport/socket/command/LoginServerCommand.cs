﻿using HelloWorld.code.common.module.core.transport.socket.response;

namespace HelloWorld.code.common.module.core.transport.socket.command {
    internal class LoginServerCommand : ServerCommandBase<LoginResponseData> {
        public override string CommandType => ServerCommandType.LOGIN;

        public LoginServerCommand(object parameters) : base(parameters) {

        }

        protected override ServerCommandBase<LoginResponseData> GetCurrentInstance() {
            return this;
        }
    }
}
