﻿using HelloWorld.code.common.module.core.transport.socket.response;

namespace HelloWorld.code.common.module.core.transport.socket.command {
    internal class GetLocationServerCommand : ServerCommandBase<MakeLocationResponseData> {
        public override string CommandType => ServerCommandType.GET_LOCATION;

        public GetLocationServerCommand(object parameters) : base(parameters) {

        }

        protected override ServerCommandBase<MakeLocationResponseData> GetCurrentInstance() {
            return this;
        }
    }
}
