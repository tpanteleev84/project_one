﻿using HelloWorld.code.common.module.core.transport.socket.datavo;
using HelloWorld.code.common.module.core.transport.socket.response;

namespace HelloWorld.code.common.module.core.transport.socket.command {
    internal class GetUserDataServerCommand : ServerCommandBase<GetUserDataResponseData> {
        public override string CommandType => ServerCommandType.GET_USER_DATA;

        public GetUserDataServerCommand(object parameters) : base(parameters) {

        }

        protected override ServerCommandBase<GetUserDataResponseData> GetCurrentInstance() {
            return this;
        }
    }
}
