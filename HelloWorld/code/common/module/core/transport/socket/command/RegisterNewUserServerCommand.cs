﻿using HelloWorld.code.common.module.core.transport.socket.response;

namespace HelloWorld.code.common.module.core.transport.socket.command {
    internal class RegisterNewUserServerCommand : ServerCommandBase<RegisterNewUserResponseData> {
        public override string CommandType => ServerCommandType.REGISTER_NEW_USER;

        public RegisterNewUserServerCommand(object parameters) : base(parameters) {

        }

        protected override ServerCommandBase<RegisterNewUserResponseData> GetCurrentInstance() {
            return this;
        }
    }
}
