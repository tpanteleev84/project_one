﻿namespace HelloWorld.code.common.module.core.transport.socket.command {

    class ServerCommandType {
        public const string REGISTER_NEW_USER = "register_new_user";
        public const string LOGIN = "login";
        public const string GET_USER_DATA = "get_user";
        public const string GET_SCENE = "get_scene";
        public const string GET_LOCATION = "get_location";
    }
}
