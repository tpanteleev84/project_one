﻿using HelloWorld.code.common.module.core.transport.socket.response;

namespace HelloWorld.code.common.module.core.transport.socket.command {
    internal class GetSceneServerCommand : ServerCommandBase<MakeSceneResponseData> {
        public override string CommandType => ServerCommandType.GET_SCENE;

        public GetSceneServerCommand(object parameters) : base(parameters) {

        }

        protected override ServerCommandBase<MakeSceneResponseData> GetCurrentInstance() {
            return this;
        }
    }
}
