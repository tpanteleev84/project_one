﻿using System.Dynamic;

namespace HelloWorld.code.common.module.core.transport.socket {
    abstract class ServerCommandResponseDataBase {
        public bool success = false;
        public string error = "";

        public dynamic data = new ExpandoObject();

        abstract internal void Parse();
    }
}
