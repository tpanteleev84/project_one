﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloWorld.code.common.module.core.transport.socket {
    interface ISocketClient {
        void SendServerRequest(IServerCommand command);
    }
}
