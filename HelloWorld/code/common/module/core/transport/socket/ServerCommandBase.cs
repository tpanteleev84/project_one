﻿using Newtonsoft.Json;
using System;

namespace HelloWorld.code.common.module.core.transport.socket {
    //TODO: Command response handler is part of model or transport special class 
    abstract class ServerCommandBase<T> : EventArgs, IServerCommand where T : ServerCommandResponseDataBase {
        public event EventHandler CommandCompleted;

        public T Response { get => _responce; }
        abstract public string CommandType { get; }
        protected T _responce;
        private object parameters = null;

        protected virtual void OnCommandCompleted() {
            EventHandler handler = CommandCompleted;
            handler?.Invoke(this, EventArgs.Empty);
        }

        public ServerCommandBase(object parameters) {
            this.parameters = parameters;
        }

        virtual protected ServerCommandBase<T> GetCurrentInstance() {
            return this;
        }

        public string CommandString {
            get {
                object commandObject = new {
                    commandType = CommandType,
                    parameters = parameters
                };

                return JsonConvert.SerializeObject(commandObject);
            }
        }

        public void ParseResponse(string jsonResponse) {
            _responce = JsonConvert.DeserializeObject<T>(jsonResponse);
            _responce.Parse();
            OnCommandCompleted();
        }
    }
}
