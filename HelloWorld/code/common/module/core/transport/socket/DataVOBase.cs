﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Reflection;

namespace HelloWorld.code.common.module.core.transport.socket {
    internal class DataVOBase {

        public void Parse(ExpandoObject dataObject) {
            if (dataObject == null)
                return;

            IDictionary<string, object> dataObjectPropertyList = (IDictionary<string, object>)dataObject;
            FieldInfo[] fieldInfos = this.GetType().GetFields();

            foreach (FieldInfo fieldInfo in fieldInfos) {
                //TODO: test if else
                if (dataObjectPropertyList.Keys.Contains(fieldInfo.Name))
                    fieldInfo.SetValue(this, FixJsonIntConverting(fieldInfo.FieldType, dataObjectPropertyList[fieldInfo.Name]));
            }
        }

        private object FixJsonIntConverting(Type targetType, object value) {
            if (value is Int64) {
                if ((Int64)value > Int32.MinValue && (Int64)value < Int32.MaxValue) {
                    if (targetType == typeof(int))
                        return Convert.ToInt32(value);
                    else if (targetType == typeof(uint))
                        return Convert.ToUInt32(value);
                }
            }

            return value;
        }
    }
}
