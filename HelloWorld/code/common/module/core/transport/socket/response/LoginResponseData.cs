﻿using HelloWorld.code.common.module.core.transport.socket.datavo;

namespace HelloWorld.code.common.module.core.transport.socket.response {
    class LoginResponseData : ServerCommandResponseDataBase {
        public readonly UserDataVO User = new UserDataVO();
        public readonly SessionDataVO Session = new SessionDataVO();

        internal override void Parse() {
            User.Parse(data.user);
            Session.Parse(data.session);
        }
    }
}
