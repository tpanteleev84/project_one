﻿using HelloWorld.code.common.module.core.transport.socket.datavo;

namespace HelloWorld.code.common.module.core.transport.socket.response {
    internal class RegisterNewUserResponseData : ServerCommandResponseDataBase {
        public readonly UserDataVO User = new UserDataVO();

        internal override void Parse() {
            User.Parse(data.user);
        }
    }
}
