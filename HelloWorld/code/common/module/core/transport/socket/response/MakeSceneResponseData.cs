﻿using HelloWorld.code.common.module.core.transport.socket.datavo;

namespace HelloWorld.code.common.module.core.transport.socket.response {
    class MakeSceneResponseData : ServerCommandResponseDataBase {
        public readonly SceneDataVO Scene = new SceneDataVO();

        internal override void Parse() {
            Scene.Parse(data.scene);
        }
    }
}
