﻿using HelloWorld.code.common.module.core.transport.socket.datavo;

namespace HelloWorld.code.common.module.core.transport.socket.response {
    class MakeLocationResponseData : ServerCommandResponseDataBase {
        public readonly LocationDataVO Location = new LocationDataVO();

        internal override void Parse() {
            Location.Parse(data.location);
        }
    }
}
