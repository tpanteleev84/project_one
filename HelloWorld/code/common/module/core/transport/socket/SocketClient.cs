﻿using HelloWorld.code.util;
using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace HelloWorld.code.common.module.core.transport.socket {
    internal class SocketClient : ISocketClient {
        private const string HOST = "127.0.0.1";
        private const int PORT = 7000;
        private const int MESSSAGE_MAX_SIZE = 2048;

        private Socket sender = null;

        private IPHostEntry ipHost = null;
        private IPAddress ipAddr = null;
        private IPEndPoint localEndPoint = null;

        public SocketClient() {
            ipHost = Dns.GetHostEntry(HOST);
            ipAddr = ipHost.AddressList[0];
            localEndPoint = new IPEndPoint(ipAddr, PORT);

            // Creation TCP/IP Socket using  
            // Socket Class Costructor 
            try {
                sender = new Socket(ipAddr.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            } catch (Exception e) {
                CustomConsole.WriteError(e.ToString());
            }
        }

        public void Connect() {
            try {
                sender.Connect(localEndPoint);
                CustomConsole.WritePlainText(String.Format("SocketClient: Socket connected to -> {0} ", sender.RemoteEndPoint.ToString()));
            } catch (SocketException se) {
                CustomConsole.WriteError(String.Format("SocketClient: SocketException : {0}", se.ToString()));
            }
        }

        public void SendServerRequest(IServerCommand command) {
            try {
                // Creation of messagge that 
                // we will send to Server 
                byte[] messageSent = Encoding.ASCII.GetBytes(command.CommandString);
                int byteSent = sender.Send(messageSent);

                // Data buffer 
                byte[] messageReceived = new byte[MESSSAGE_MAX_SIZE];

                // We receive the messagge using  
                // the method Receive(). This  
                // method returns number of bytes 
                // received, that we'll use to  
                // convert them to string 
                int byteRecv = sender.Receive(messageReceived);
                string jsonResponse = Encoding.ASCII.GetString(messageReceived, 0, byteRecv);
                command.ParseResponse(jsonResponse);
                CustomConsole.WritePlainText(String.Format("SocketClient: Message from Server -> {0}", jsonResponse));

                // Close Socket using  
                // the method Close() 
                sender.Shutdown(SocketShutdown.Both);
                sender.Close();
            }

            // Manage of Socket's Exceptions 
            catch (ArgumentNullException ane) {
                CustomConsole.WriteError(String.Format("SocketClient: ArgumentNullException : {0}", ane.ToString()));
            } catch (SocketException se) {
                CustomConsole.WriteError(String.Format("SocketClient: SocketException : {0}", se.ToString()));
            } catch (Exception e) {
                CustomConsole.WriteError(String.Format("SocketClient: Unexpected exception : {0}", e.ToString()));
            }
        }
    }
}
