﻿using HelloWorld.test;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;

namespace HelloWorld.code.common.module.core.transport.xml {
    internal class SerializationList<T> : ITestSerializable {
        [XmlElement(ElementName = "serializable_list")]
        public List<T> list { get; set; }

        public void DisplayToConsole() {
            foreach (T listItem in list)
                if (listItem is ITestSerializable)
                    ((ITestSerializable)listItem).DisplayToConsole();
        }
    }
}
