﻿using HelloWorld.contentserver.generator.mob;

namespace HelloWorld.code.module.core.units {
    internal class ForestUnitFactory : IUnitFactory {
        public ForestUnitFactory() {
        }

        IBattleUnit IUnitFactory.CreateEnemyMob() {
            return null;
        }

        INPC IUnitFactory.CreateNPCMob() {
            return null;
        }
    }
}
