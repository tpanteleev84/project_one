﻿using HelloWorld.contentserver.generator.mob;

namespace HelloWorld.code.module.core.units {
    internal class PlaneUnitFactory : IUnitFactory {
        public PlaneUnitFactory() {
        }

        IBattleUnit IUnitFactory.CreateEnemyMob() {
            return null;
        }

        INPC IUnitFactory.CreateNPCMob() {
            return null;
        }
    }
}
