﻿using HelloWorld.contentserver.generator.mob;

namespace HelloWorld.code.module.core.units {
    internal class RockUnitFactory : IUnitFactory {
        public RockUnitFactory() {
        }

        IBattleUnit IUnitFactory.CreateEnemyMob() {
            return null;
        }

        INPC IUnitFactory.CreateNPCMob() {
            return null;
        }
    }
}
