﻿using HelloWorld.code.common.module.controller.command;
using HelloWorld.code.common.module.model;
using HelloWorld.code.common.module.model.items;
using HelloWorld.code.common.module.model.location;
using HelloWorld.code.pattern.command;
using HelloWorld.code.pattern.ommand;
using HelloWorld.code.util;

namespace HelloWorld.code.common.module.controller {
    class GameController {
        private GameModel model = null;
        private Invoker commandInvoker = null;

        public GameController(GameModel model) {
            this.model = model;

            commandInvoker = new Invoker();
        }

        public void UseItem(string data) {
            ItemType type = ItemType.UNKNOWN;
            switch (type) {
                case ItemType.WEAPON:
                    AddCommandToQueue(new UseWeaponCommand(data, model));
                    break;
                case ItemType.SUPPLY:
                    AddCommandToQueue(new UseSuppliesCommand(data, model));
                    break;
                default:
                    CustomConsole.WritePlainText("GameController: can't use item with type " + type);
                    break;
            }
        }
        public void GoToTheNextScene(DirectionType directionType) {
            AddCommandToQueue(new GoToTheNextSceneCommand(null, model, directionType));
        }
        public void SpeakWithNPC(string data) {
            AddCommandToQueue(new SpeakWithNPCCommand(data, model));
        }
        public void SearhTheCorps(string data) {
            AddCommandToQueue(new SearchTheCorpseCommand(data, model));
        }
        private void AddCommandToQueue(Command command) {
            commandInvoker.AddCommandToQueue(command);
            commandInvoker.Run();
        }
    }
}
