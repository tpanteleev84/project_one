﻿using HelloWorld.code.common.module.model;
using HelloWorld.code.common.module.model.location;
using HelloWorld.code.pattern.ommand;

namespace HelloWorld.code.common.module.controller.command {
    class GoToTheNextSceneCommand : Command {
        private readonly string data = "";
        private readonly GameModel receiver = null;
        private readonly DirectionType directionType;

        public GoToTheNextSceneCommand(string data, GameModel receiver, DirectionType directionType) {
            this.data = data;
            this.receiver = receiver;
            this.directionType = directionType;
        }
        public override void Execute() {
            //TODO: use real data
            receiver.GoToTheNextScene(directionType);
        }
    }
}
