﻿namespace HelloWorld.code.common.module.controller.command {
    class VirtualInterfaceCommandType {
        public const string USE_ITEM = "use";
        public const string SEARCH_THE_CORPS = "search";
        public const string GO_TO_THE_NEXT_SCENE = "go";
        public const string SPEAK_WITH_NPC = "speak";
        public const string ATTACK_THE_ENEMY = "attack";
    }
}
