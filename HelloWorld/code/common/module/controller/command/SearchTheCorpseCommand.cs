﻿using HelloWorld.code.common.module.model;
using HelloWorld.code.pattern.ommand;

namespace HelloWorld.code.common.module.controller.command {
    class SearchTheCorpseCommand : Command {
        private string data = "";
        private GameModel receiver = null;

        public SearchTheCorpseCommand(string data, GameModel receiver) {
            this.data = data;
            this.receiver = receiver;
        }
        public override void Execute() {
            //TODO use real data
            receiver.SearchTheCorpse(0);
        }
    }
}
