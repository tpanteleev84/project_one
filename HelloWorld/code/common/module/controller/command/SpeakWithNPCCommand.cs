﻿using HelloWorld.code.common.module.model;
using HelloWorld.code.pattern.ommand;

namespace HelloWorld.code.common.module.controller.command {
    class SpeakWithNPCCommand : Command {
        private string data = "";
        private GameModel receiver = null;

        public SpeakWithNPCCommand(string data, GameModel receiver) {
            this.data = data;
            this.receiver = receiver;
        }
        public override void Execute() {
            //TODO use real data
            receiver.SearchTheCorpse(0);
        }
    }
}
