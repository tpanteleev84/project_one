﻿using HelloWorld.code.common.module.model;
using HelloWorld.code.pattern.ommand;

namespace HelloWorld.code.common.module.controller.command {
    class UseSuppliesCommand : Command {
        private string data = "";
        private GameModel receiver = null;

        public UseSuppliesCommand(string data, GameModel receiver) {
            this.data = data;
            this.receiver = receiver;
        }
        public override void Execute() {
            //TODO use real data
            receiver.UseSupplies(0);
        }
    }
}
