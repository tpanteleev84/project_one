﻿using HelloWorld.code.common.library.location;
using HelloWorld.code.common.module.core.transport.socket.datavo;
using HelloWorld.code.util;
using System.Collections.Generic;

namespace HelloWorld.code.common.module.model.location {
    internal class LocationModel : ModelBase {
        public uint Id { get; private set; }
        public uint Uid { get; private set; }
        public LocationData LocationData { get; private set; }
        private Dictionary<uint, SceneModel> ScenesById = null; //TODO: Iterator
        private LocationType type = LocationType.UNKNOWN;
        private uint currentSceneId = 0;

        public LocationType Type { get { return type; } } //TODO part of location data, not model

        //TODO: Dynamic data. Static data if fake
        public void Init(LocationData locationData) {
            currentSceneId = 0;

            Id = locationData.Id;
            type = locationData.Type;
            ScenesById = new Dictionary<uint, SceneModel>();
            foreach (SceneData sceneData in locationData.SceneDatas) {
                ScenesById[sceneData.Id] = new SceneModel(sceneData);
            }
        }

        public SceneModel CurrentScene {
            get {
                return GetSceneById(currentSceneId);
            }
        }

        public void Update(LocationDataVO locationDataVO) {
            Id = locationDataVO.id;
            Uid = locationDataVO.uid;
            //TODO: To user model
            currentSceneId = locationDataVO.currentSceneId;
            OnModelChanged();
        }

        public SceneModel GetSceneById(uint id) {
            //TODO: Or fake stub, or handle exeption
            if (!ScenesById.ContainsKey(id))
                CustomConsole.WriteError("LocationModel id=" + Id + ": SceneModel with Id=" + id + " doesn't exist");
            else
                return ScenesById[currentSceneId];

            return null;
        }
    }
}
