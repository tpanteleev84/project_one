﻿namespace HelloWorld.code.common.module.model.location {

    internal enum DirectionType {
        UNKNOWN = 0,
        LEFT = 1,
        RIGHT = 2,
        FORWARD = 3,
        TURNBACK = 4,
        UP = 5,
        DOWN = 6
    }
}