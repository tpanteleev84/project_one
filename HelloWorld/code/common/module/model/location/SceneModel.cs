﻿using HelloWorld.code.common.library.location;
using HelloWorld.code.common.module.core.transport.socket.datavo;
using HelloWorld.code.common.module.model.unit;
using HelloWorld.code.pattern.generator.factoryMethod;
using System;
using System.Collections.Generic;

namespace HelloWorld.code.common.module.model.location {
    internal class SceneModel : ModelBase {
        public uint Id { get; private set; }
        public uint Uid { get; private set; }
        public string SceneDescription { get; private set; }

        private SceneType type = SceneType.UNKNOWN;
        private List<UnitBaseModel> units = null;
        private List<IItem> loot = null;
        private Dictionary<DirectionType, SceneModel> directions = null;

        public SceneModel(SceneData sceneData) {
            this.type = sceneData.Type;
            this.units = new List<UnitBaseModel>();
            this.loot = new List<IItem>();
            this.directions = new Dictionary<DirectionType, SceneModel>();
        }

        public Boolean HasLoot() {
            return loot != null && loot.Count > 0;
        }

        public Boolean HasEnemy() {
            foreach (UnitBaseModel unit in units) {
                if (unit.GetType() == typeof(EnemyUnitModel))
                    return true;
            }

            return false;
        }

        public Boolean HasNPC() {
            foreach (UnitBaseModel unit in units) {
                if (unit.GetType() == typeof(NPCUnitModel))
                    return true;
            }

            return false;
        }

        public Boolean HasQuest() {
            foreach (UnitBaseModel unit in units) {
                if (unit.GetType() == typeof(NPCUnitModel) && ((NPCUnitModel)unit).GetQuests().Count > 0)
                    return true;
            }

            return false;
        }

        public List<DirectionType> GetAvailableDirections() {
            List<DirectionType> result = new List<DirectionType>();

            foreach (DirectionType directionType in directions.Keys) {
                result.Add(directionType);
            }

            return result;
        }

        public SceneModel GetSceneByDirectionType(DirectionType directionType) {
            if (directions.ContainsKey(directionType)) {
                return directions[directionType];
            }

            return null;
        }

        public void Update(SceneDataVO sceneDataVO) {
            Id = sceneDataVO.id;
            Uid = sceneDataVO.uid;
            SceneDescription = sceneDataVO.description;
            OnModelChanged();
        }
    }
}
