﻿namespace HelloWorld.code.common.module.model.items {
    public enum ItemType {
        UNKNOWN = 0,
        WEAPON = 1,
        SUPPLY = 2
    }
}
