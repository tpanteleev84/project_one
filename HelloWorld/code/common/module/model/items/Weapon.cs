﻿using HelloWorld.code.pattern.generator.factoryMethod;

namespace HelloWorld.code.common.module.model.items {
    class Weapon : IItem {
        private int damage = 0;
        private int ammo = 0;

        public Weapon() {

        }

        public Weapon(Weapon sourceWeapon) {

        }

        public IItem Clone() {
            return new Weapon(this);
        }
    }
}
