﻿using HelloWorld.code.pattern.generator.factoryMethod;

namespace HelloWorld.code.common.module.model.items {
    abstract class ItemBase : IItem {
        private string name = "";
        private string description = "";
        private string effect = "";
        public abstract IItem Clone();
    }
}
