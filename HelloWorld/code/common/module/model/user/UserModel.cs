﻿using HelloWorld.code.common.module.core.transport.socket.datavo;
using HelloWorld.code.common.module.model.unit;
using HelloWorld.code.common.module.model.user;
using HelloWorld.contentserver.generator.mob;
using System;

namespace HelloWorld.code.common.module.model {
    class UserModel : ModelBase, IBattleUnit {
        public uint Uid { get; private set; }
        private UnitStatusType status = UnitStatusType.UNKNOWN;
        private UserBackpackModel backpack = null;
        private string userData = null; //TODO static user data? (entering name)
        private int health = 0;
        public int Health { get { return health; } }
        public uint CurrentLocationId { get; private set; }
        public uint CurrentSceneId { get; private set; }
        public UserBackpackModel Backpack { get { return backpack; } }

        public string Name { get; private set; }

        public string Email { get; private set; }

        public UnitStatusType Status => status;

        public string Description => throw new NotImplementedException();

        public void DealDamage(int damage) {
            throw new NotImplementedException();
        }

        public void RestoreHP(int hp) {
            throw new NotImplementedException();
        }

        internal void Update(UserDataVO user) {
            Uid = user.uid;
            Name = user.name;
            Email = user.email;
            CurrentLocationId = user.location_id;
            CurrentSceneId = user.scene_id;
            //status = (UnitStatusType)user.status;
            OnModelChanged();
        }

        /*
        public void Update(string userData) {
            this.userData = userData;

            backpack = new UserBackpackModel(5);
            backpack.init(userData);
        }*/
    }
}
