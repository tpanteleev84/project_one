﻿using HelloWorld.code.common.module.core.transport.socket.datavo;

namespace HelloWorld.code.common.module.model.session {
    class SessionModel : ModelBase {
        public uint Uid { get; private set; }
        public uint UserUid { get; private set; }
        public uint ServerTime { get; private set; }
        public string Token { get; private set; }

        public void Update(SessionDataVO session) {
            Uid = session.uid;
            UserUid = session.user_uid;
            ServerTime = session.serverTime;
            Token = session.token;
            OnModelChanged();
        }
    }
}
