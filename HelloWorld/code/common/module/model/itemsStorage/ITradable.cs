﻿using HelloWorld.code.pattern.generator.factoryMethod;

namespace HelloWorld.code.common.module.model.itemsStorage {
    interface ITradable {
        IItem BuyItem(string itemType);
        int SellItem(IItem item);

    }
}
