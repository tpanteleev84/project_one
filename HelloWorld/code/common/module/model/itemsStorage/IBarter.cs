﻿using HelloWorld.code.pattern.generator.factoryMethod;

namespace HelloWorld.code.common.module.model.itemsStorage {
    interface IBarter {
        void AddItem(IItem item);
        void RemoveItem(IItem item);
        void RemoveItem(int slotIndex);
        void ReplaceItem(IItem addingItem, IItem removingItem);
        void ReplaceItem(IItem addingItem, int slotIndex);

    }
}
