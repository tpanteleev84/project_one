﻿using HelloWorld.code.pattern.generator.factoryMethod;
using System.Collections.Generic;

namespace HelloWorld.code.common.module.model.itemsStorage {
    class StorageModel : ModelBase, IBarter {
        private List<IItem> items = null;
        private int capacity = -1;

        public StorageModel(int capacity) {
            items = new List<IItem>();
            this.capacity = capacity;
        }

        public void AddItem(IItem item) {
            items.Add(item);
        }

        public void RemoveItem(IItem item) {
            items.Remove(item);
        }

        public void RemoveItem(int slotIndex) {
            items.RemoveAt(slotIndex);
        }

        public void ReplaceItem(IItem addingItem, IItem removingItem) {
            int slotNum = items.IndexOf(removingItem);
            if (slotNum != -1)
                items.Insert(slotNum, addingItem);
        }

        public void ReplaceItem(IItem addingItem, int slotIndex) {

        }
    }
}
