﻿using System;

namespace HelloWorld.code.common.module.model {
    internal class ModelBase {
        public event EventHandler ModelChanged;
        
        protected virtual void OnModelChanged() {
            EventHandler handler = ModelChanged;
            handler?.Invoke(this, EventArgs.Empty);
        }
    }
}
