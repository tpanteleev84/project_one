﻿namespace HelloWorld.code.common.module.model.unit {
    public enum UnitType {
        UNKNOWN = 0,
        NPC = 1,
        ENEMY_LAND = 2,
        ENEMY_WATERFOWL = 3,
        ENEMY_FLYING = 4,
        ENEMY_UNDERGROUND = 5
    }
}
