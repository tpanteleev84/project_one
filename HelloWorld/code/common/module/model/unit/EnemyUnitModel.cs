﻿using HelloWorld.contentserver.generator.mob;
using System;

namespace HelloWorld.code.common.module.model.unit {
    class EnemyUnitModel : UnitBaseModel, IBattleUnit {
        private int _minDamage = 0;
        private int _maxDamage = 0;
        public EnemyUnitModel(string data) : base(data) {
            _minDamage = new Random().Next(10, 20);
            _maxDamage = new Random().Next(10, 20);
        }

        public void DealDamage(int damage) {
            throw new NotImplementedException();
        }

        public void RestoreHP(int hp) {
            throw new NotImplementedException();
        }
    }
}
