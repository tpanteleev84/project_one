﻿namespace HelloWorld.code.common.module.model.unit {
    enum UnitStatusType {
        UNKNOWN = 0,
        ALIVE = 1,
        DEATH = 2
    }
}
