﻿using HelloWorld.contentserver.generator.mob;

namespace HelloWorld.code.common.module.model.unit {
    class UnitBaseModel : ModelBase, IUnit {
        private string name = "";
        private string description = "";
        private UnitStatusType status = UnitStatusType.UNKNOWN;

        public UnitBaseModel(string data) //add real data
        {
            name = data;
            description = data;
            status = UnitStatusType.UNKNOWN;
        }

        public string Name => name;

        public string Description => description;

        public UnitStatusType Status => status;
    }
}