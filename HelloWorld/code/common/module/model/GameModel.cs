﻿using HelloWorld.code.common.library;
using HelloWorld.code.common.library.location;
using HelloWorld.code.common.module.core.transport.socket;
using HelloWorld.code.common.module.core.transport.socket.command;
using HelloWorld.code.common.module.model.location;
using HelloWorld.code.common.module.model.session;
using HelloWorld.code.util;
using System;
using System.Collections.Generic;

namespace HelloWorld.code.common.module.model {
    class GameModel //Command receiver
    {
        public event EventHandler ModelInited;

        private ServerCommandManager serverCommandManager = null;

        public SessionModel SessionModel { get; private set; }
        public UserModel UserModel { get; private set; }
        public LocationModel LocationModel { get; private set; }
        public Dictionary<uint, LocationModel> locationById = null;

        public GameModel(SocketClient socketClient) {
            locationById = new Dictionary<uint, LocationModel>();

            UserModel = new UserModel();
            LocationModel = new LocationModel();
            SessionModel = new SessionModel();

            serverCommandManager = new ServerCommandManager(this, socketClient);
        }

        public void Init() {
            GetUserDataServerCommand getUserDataServerCommand = new GetUserDataServerCommand(
                    new {
                        user_uid = SessionModel.UserUid
                    }
                );

            UserModel.ModelChanged += OnUserModelInited;
            serverCommandManager.ExecuteCommand(getUserDataServerCommand);
        }

        private void OnUserModelInited(object sender, EventArgs args) {
            UserModel.ModelChanged -= OnUserModelInited;

            LocationData locationData = GameLibrary.Instance.Locations.GetLocationById(UserModel.CurrentLocationId);
            LocationModel.Init(locationData);

            GetLocationServerCommand getLocationServerCommand = new GetLocationServerCommand(
                    new {
                        user_id = UserModel.Uid,
                        location_id = UserModel.CurrentLocationId
                    }
                );

            LocationModel.ModelChanged += OnLocationModelInited;
            serverCommandManager.ExecuteCommand(getLocationServerCommand);
        }

        private void OnLocationModelInited(object sender, EventArgs args) {
            LocationModel.ModelChanged -= OnLocationModelInited;
            GetSceneServerCommand getSceneServerCommand = new GetSceneServerCommand(
                     new {
                         location_uid = LocationModel.Uid,
                         scene_id = UserModel.CurrentSceneId
                     }
                 );

            serverCommandManager.ExecuteCommand(getSceneServerCommand);
            OnModelInited();
    }

        public void RegisterNewUser(string email, string name, string password) {
            object parameters = new {
                email = email,
                name = name,
                password = password
            };

            RegisterNewUserServerCommand serverCommand = new RegisterNewUserServerCommand(parameters);
            serverCommandManager.ExecuteCommand(serverCommand);
        }

        public void Login(string email, string password) {
            object parameters = new {
                email = email,
                password = password
            };

            LoginServerCommand serverCommand = new LoginServerCommand(parameters);
            serverCommandManager.ExecuteCommand(serverCommand);
        }

        public void SearchTheCorpse(int unitId) {
            throw new NotImplementedException();
        }

        internal void UseSupplies(int itemId) {
            throw new NotImplementedException();
        }

        internal void UseWeapon(int itemId, int unitId) {
            throw new NotImplementedException();
        }

        internal void GoToTheNextScene(DirectionType directionType) {
            SceneModel nextSceneModel = LocationModel.CurrentScene.GetSceneByDirectionType(directionType);
            if (nextSceneModel == null) {
                CustomConsole.WriteError("GameModel: scene with id = " + LocationModel.CurrentScene +
                    " have not related scene by dyrection = " + directionType);
            }

            //TODO: Server request
            CustomConsole.WriteWarning("GameModel: GoToTheNextScene server request doesn't exist");
        }

        public LocationModel GetLocationById(uint id) {
            //TODO: Or fake stub, or handle exeption
            if (!locationById.ContainsKey(id))
                CustomConsole.WriteError("GameModel: LocationModel with Id=" + id + " doesn't exist");
            else
                return locationById[id];

            return null;
        }

        private void OnModelInited() {
            EventHandler handler = ModelInited;
            handler?.Invoke(this, EventArgs.Empty);
        }
    }
}
