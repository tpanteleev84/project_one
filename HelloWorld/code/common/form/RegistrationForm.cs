﻿using HelloWorld.code.common.module.core.transport.socket.command;
using HelloWorld.code.common.module.model;
using HelloWorld.code.util;
using System;

namespace HelloWorld.code.common.form {

    internal class RegistrationForm {
        public delegate void RegisterCompletedEventHandler(RegistrationForm source, EventArgs args);
        public event RegisterCompletedEventHandler RegisterCompleted;
        private GameModel gameModel;

        protected virtual void OnRegisterCompleted() {
            RegisterCompletedEventHandler handler = RegisterCompleted;
            handler?.Invoke(this, EventArgs.Empty);
        }

        public RegistrationForm(GameModel gameModel) {
            this.gameModel = gameModel;
        }

        public void Show() {
            CustomConsole.WritePlainText("Enter email: ");
            string email = Console.ReadLine();
            CustomConsole.WritePlainText("Enter character name: ");
            string name = Console.ReadLine();
            CustomConsole.WritePlainText("Enter password: ");
            string password = Console.ReadLine();

            gameModel.UserModel.ModelChanged += onRegisterNewUserCompleted;
            gameModel.RegisterNewUser(email, name, password);
        }

        private void onRegisterNewUserCompleted(object source, EventArgs args) {
            gameModel.UserModel.ModelChanged -= onRegisterNewUserCompleted;
            OnRegisterCompleted();
        }
    }
}
