﻿using HelloWorld.code.common.module.model;
using HelloWorld.code.util;
using System;

namespace HelloWorld.code.common.form {
    internal class LoginForm {
        public delegate void LoginCompletedEventHandler(LoginForm source, EventArgs args);
        public event LoginCompletedEventHandler LoginCompleted;

        private GameModel gameModel;

        protected virtual void OnLoginCompleted() {
            LoginCompletedEventHandler handler = LoginCompleted;
            handler?.Invoke(this, EventArgs.Empty);
        }

        public LoginForm(GameModel gameModel) {
            this.gameModel = gameModel;
        }

        public void Show() {
            CustomConsole.WritePlainText("Enter email: ");
            string email = Console.ReadLine();
            CustomConsole.WritePlainText("Enter password: ");
            string password = Console.ReadLine();
            
            gameModel.SessionModel.ModelChanged += onSessionModelChanged;
            gameModel.Login(email, password);
        }

        private void onSessionModelChanged(object source, EventArgs args) {
            gameModel.SessionModel.ModelChanged -= onSessionModelChanged;
            OnLoginCompleted();
        }
    }
}
