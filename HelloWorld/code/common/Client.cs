﻿using HelloWorld.code.common.library;
using HelloWorld.code.common.module.controller;
using HelloWorld.code.common.module.core.transport.socket;
using HelloWorld.code.common.module.model;
using HelloWorld.code.common.module.view;
using HelloWorld.code.util;
using System;

namespace HelloWorld.code.pattern.command {
    class Client {
        private GameModel gameModel = null;
        private GameController gameController = null;
        private GameView gameView = null;

        public Client(GameModel gameModel) {
            this.gameModel = gameModel;
            gameController = new GameController(gameModel);
            gameView = new GameView(gameModel, gameController);
        }

        public void Start() {
            Console.SetWindowSize(200, 50);

            InitLibrary();
            InitGameModel();
        }

        private void InitLibrary() {
            string configPath = "../../config/";
            CustomConsole.WritePlainText("configPath: " + configPath);
            GameLibrary.Instance.Terrains.parseDataFromXML(configPath + "terrains.xml");
            GameLibrary.Instance.Locations.parseDataFromXML(configPath + "locations.xml");
        }

        private void InitGameModel() {
            gameModel.Init();
            gameModel.ModelInited += OnGameModelInited;
        }

        private void OnGameModelInited(object sender, EventArgs args) {
            gameModel.ModelInited -= OnGameModelInited;
            gameView.InitLocation();
            gameView.RunGUI();
        }
    }
}
