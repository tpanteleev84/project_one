﻿using HelloWorld.code.common.library.items;
using HelloWorld.code.common.library.location;
using HelloWorld.code.common.library.terrain;
using System;

namespace HelloWorld.code.common.library {
    class GameLibrary {
        private static GameLibrary instance = null;

        public static GameLibrary Instance {
            get {
                if (instance == null)
                    instance = new GameLibrary();

                return instance;
            }
        }

        public LocationLibrary Locations { get; private set; }
        public TerrainLibrary Terrains { get; private set; }
        public ItemsLibrary Items { get; private set; }

        public GameLibrary() {
            if (instance != null)
                throw new Exception("LocationLibrary singleton object already have a instance");

            Locations = new LocationLibrary();
            Terrains = new TerrainLibrary();
            Items = new ItemsLibrary();
        }
    }
}
