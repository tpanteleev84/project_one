﻿using System.Xml.Serialization;

namespace HelloWorld.code.common.library.location {
    public enum LocationType {
        UNKNOWN = 0,
        [XmlEnum(Name = "plane")]
        PLANE = 1,
        [XmlEnum(Name = "forest")]
        FOREST = 2,
        [XmlEnum(Name = "island")]
        ISLAND = 3,
        [XmlEnum(Name = "rock")]
        ROCK = 4
    }
}
