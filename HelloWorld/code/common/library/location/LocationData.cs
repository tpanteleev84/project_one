﻿using HelloWorld.code.util;
using HelloWorld.test;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;

namespace HelloWorld.code.common.library.location {
    [Serializable]
    public class LocationData : ITestSerializable {
        [XmlAttribute("id")]
        public uint Id { get; set; }
        [XmlAttribute("type")]
        public LocationType Type { get; set; }
        [XmlElement("name")]
        public string Name { get; set; }
        [XmlElement("description")]
        public string Description { get; set; }
        [XmlArray("scenes")]
        [XmlArrayItem("scene")]
        public List<SceneData> SceneDatas { get; set; }

        public void DisplayToConsole() {
            string message = "LocationData: Id=" + Id + " Name=" + Name + " Description=" + Description;
            CallingStackList.Instance.PrintMessageWithTabs(message, "DisplayToConsole");

            foreach (SceneData sceneData in SceneDatas)
                sceneData.DisplayToConsole();
        }

        public SceneData GetSceneById(uint sceneId) {
            foreach (SceneData sceneData in SceneDatas)
                if (sceneData.Id == sceneId)
                    return sceneData;

            CustomConsole.WriteError("LocationLibrary: SceneData with id = " + sceneId + " doesn't exist into LocationData with id = " + Id);
            return SceneDatas.First();
        }
    }
}
