﻿using HelloWorld.code.common.module.model.location;
using HelloWorld.code.util;
using HelloWorld.test;
using System;
using System.Xml.Serialization;

namespace HelloWorld.code.common.library.location {
    [Serializable]
    public class SceneData : ITestSerializable {
        [XmlAttribute(AttributeName = "id")]
        public uint Id { get; set; }
        [XmlAttribute(AttributeName = "type")]
        public SceneType Type { get; set; }
        [XmlElement(ElementName = "name")]
        public string Name { get; set; }
        [XmlElement(ElementName = "description")]
        public string Description { get; set; }
        [XmlArray("terrain")]
        [XmlArrayItem("terrain_element")]
        public LocationTerrainElementData[] LocationTerrainElementDatas { get; set; }

        public void DisplayToConsole() {
            string message = "SceneData Id=" + Id + " Type=" + Type + " Name=" + Name + " Description=" + Description;
            CallingStackList.Instance.PrintMessageWithTabs(message, "DisplayToConsole");

            foreach (LocationTerrainElementData locationTerrainElementData in LocationTerrainElementDatas)
                locationTerrainElementData.DisplayToConsole();
        }
    }
}
