﻿using HelloWorld.code.common.library.terrain;
using HelloWorld.code.util;
using HelloWorld.test;
using System;
using System.Xml.Serialization;

namespace HelloWorld.code.common.library.location {
    [Serializable]
    public class LocationTerrainElementData : ITestSerializable {
        [XmlAttribute(AttributeName = "type")]
        public TerrainElementType Type { get; set; }
        [XmlAttribute(AttributeName = "use_intensity")]
        public uint UseIntensity { get; set; }

        public void DisplayToConsole() {
            string message = "LocationTerrainElementData Type=" + Type + " UseIntensity=" + UseIntensity;
            CallingStackList.Instance.PrintMessageWithTabs(message, "DisplayToConsole");
        }
    }
}
