﻿using HelloWorld.test;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace HelloWorld.code.common.library.location {
    public class LocationDatas : ITestSerializable {
        [XmlElement(ElementName = "location")]
        public List<LocationData> locations { get; set; }

        public void DisplayToConsole() {
            foreach (LocationData locationData in locations)
                locationData.DisplayToConsole();
        }
    }
}
