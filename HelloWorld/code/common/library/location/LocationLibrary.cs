﻿using HelloWorld.code.util;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HelloWorld.code.common.library.location {
    public class LocationLibrary {
        private static string LOCATIONS_ROOT_XML_ELEMENT_NAME = "locations";

        private LocationDatas locationDatas;

        private Dictionary<uint, LocationData> locationById = null;
        private Dictionary<uint, Dictionary<uint, SceneData>> scenesByLocationId = null;

        public LocationLibrary() {
            locationDatas = new LocationDatas();
        }

        public LocationData GetLocationById(uint locationId) {
            if (!locationById.ContainsKey(locationId))
                CustomConsole.WriteError("LocationLibrary: LocationData with id = " + locationId + " doesn't exist");

            return locationById[locationId];
        }

        public SceneData getSceneDataById(uint locationId, uint sceneId) {
            if (!scenesByLocationId.ContainsKey(locationId))
                CustomConsole.WriteError("LocationLibrary: LocationData with id = " + locationId + " doesn't exist");
            else if (scenesByLocationId[locationId].ContainsKey(sceneId))
                CustomConsole.WriteError("LocationLibrary: SceneId with id = " + sceneId 
                    + " in Location with id = " + locationId + " doesn't exist");

            return scenesByLocationId[locationId][sceneId];
        }

        public void parseDataFromXML(string uri) {
            CustomXmlSerializer xmlSerializer = new CustomXmlSerializer(uri, LocationLibrary.LOCATIONS_ROOT_XML_ELEMENT_NAME, locationDatas);
            locationDatas = (LocationDatas)xmlSerializer.Deserialize();
            locationDatas.DisplayToConsole();

            locationById = new Dictionary<uint, LocationData>();
            scenesByLocationId = new Dictionary<uint, Dictionary<uint, SceneData>>();
            foreach (LocationData locationData in locationDatas.locations) {
                locationById.Add(locationData.Id, locationData);
                foreach (SceneData sceneData in locationData.SceneDatas) {
                    if (!scenesByLocationId.ContainsKey(locationData.Id)) {
                        scenesByLocationId.Add(locationData.Id, new Dictionary<uint, SceneData>());
                    }

                    scenesByLocationId[locationData.Id][sceneData.Id] = sceneData;
                }
            }
        }
    }
}
