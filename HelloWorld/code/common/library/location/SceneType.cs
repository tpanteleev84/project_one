﻿using System.Xml.Serialization;

namespace HelloWorld.code.common.library.location {
    public enum SceneType {
        UNKNOWN = 0,
        [XmlEnum(Name = "battle")]
        BATTLE = 1,
        [XmlEnum(Name = "story")]
        STORY = 2
    }
}
