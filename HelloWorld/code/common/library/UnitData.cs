﻿using HelloWorld.code.common.module.model.unit;

namespace HelloWorld.code.common.library {
    internal class UnitData {
        public uint Id { get; private set; }
        public UnitType Type { get; private set; }
        public string Name { get; private set; }
        public string Description { get; private set; }
    }
}
