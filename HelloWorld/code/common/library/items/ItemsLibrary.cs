﻿using HelloWorld.code.util;

namespace HelloWorld.code.common.library.items {
    internal class ItemsLibrary {
        private static string ITEMS_ROOT_XML_ELEMENT_NAME = "items";

        private ItemDatas itemDatas;

        public ItemsLibrary() {
            itemDatas = new ItemDatas();
        }

        public void parseDataFromXML(string uri) {
            CustomXmlSerializer xmlSerializer = new CustomXmlSerializer(uri, ItemsLibrary.ITEMS_ROOT_XML_ELEMENT_NAME, itemDatas);
            itemDatas = (ItemDatas)xmlSerializer.Deserialize();
            itemDatas.DisplayToConsole();
        }
    }
}
