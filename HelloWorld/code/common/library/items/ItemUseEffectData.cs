﻿using HelloWorld.test;

namespace HelloWorld.code.common.library.items {
    internal class ItemUseEffectData : ITestSerializable {
        public string Name { get; set; }
        public string Description { get; set; }
        public ItemUseEffectType Type { get; set; }

        public void DisplayToConsole() {

        }
    }
}
