﻿using HelloWorld.test;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace HelloWorld.code.common.library.items {
    internal class ItemDatas : ITestSerializable {
        [XmlElement(ElementName = "item")]
        public List<ItemData> items { get; set; }

        public void DisplayToConsole() {
            foreach (ItemData item in items)
                item.DisplayToConsole();
        }
    }
}
