﻿using System.Xml.Serialization;

namespace HelloWorld.code.common.library.items {
    public enum ItemUseEffectType {
        UNKNOWN = 0,
        [XmlEnum("damage")]
        DAMAGE = 1,
        [XmlEnum("heal")]
        HEAL = 2
    }
}
