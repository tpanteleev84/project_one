﻿using HelloWorld.code.common.module.model.items;
using HelloWorld.test;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace HelloWorld.code.common.library.items {
    internal class ItemData : ITestSerializable {
        [XmlElement("name")]
        public string Name { get; set; }
        [XmlElement("description")]
        public string Description { get; set; }
        [XmlAttribute("type")]
        public ItemType Type { get; set; }
        public List<UseEffectData> UseEffectDatas { get; set; }

        public void DisplayToConsole() {

        }
    }
}
