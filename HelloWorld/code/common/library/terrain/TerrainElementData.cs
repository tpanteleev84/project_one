﻿using HelloWorld.code.util;
using HelloWorld.test;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace HelloWorld.code.common.library.terrain {
    [Serializable]
    public class TerrainElementData : ITestSerializable {
        [XmlAttribute("type")]
        public TerrainElementType Type { get; set; }
        [XmlArray("descriptions")]
        [XmlArrayItem("description")]
        public List<String> Descriptions { get; set; }

        public void DisplayToConsole() {
            string message = "TerrainElementData: Type=" + Type + " Descriptions=" + Descriptions;
            CallingStackList.Instance.PrintMessageWithTabs(message, "DisplayToConsole");
        }
    }
}
