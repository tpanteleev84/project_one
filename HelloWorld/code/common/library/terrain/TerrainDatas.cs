﻿using HelloWorld.test;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace HelloWorld.code.common.library.terrain {
    public class TerrainDatas : ITestSerializable {
        [XmlElement(ElementName = "terrain")]
        public List<TerrainData> terrains { get; set; }

        public void DisplayToConsole() {
            foreach (TerrainData terrainData in terrains)
                terrainData.DisplayToConsole();
        }
    }
}
