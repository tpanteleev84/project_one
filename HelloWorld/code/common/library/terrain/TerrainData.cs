﻿using HelloWorld.code.common.library.location;
using HelloWorld.code.util;
using HelloWorld.test;
using System;
using System.Xml.Serialization;

namespace HelloWorld.code.common.library.terrain {
    [Serializable]
    public class TerrainData : ITestSerializable {
        [XmlAttribute("location_type")]
        public LocationType LocationType { get; set; }
        [XmlArray("terrain_elements")]
        [XmlArrayItem("terrain_element")]
        public TerrainElementData[] TerrainElementDatas { get; set; }
        public void DisplayToConsole() {
            string message = "TerrainData: LocationType=" + LocationType;
            CallingStackList.Instance.PrintMessageWithTabs(message, "DisplayToConsole");

            foreach (TerrainElementData terrainElementData in TerrainElementDatas)
                terrainElementData.DisplayToConsole();
        }
    }
}
