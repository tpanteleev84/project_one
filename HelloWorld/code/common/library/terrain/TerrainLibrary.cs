﻿using HelloWorld.code.common.library.location;
using HelloWorld.code.util;

namespace HelloWorld.code.common.library.terrain {
    public class TerrainLibrary {
        private static string TERRAINS_ROOT_XML_ELEMENT_NAME = "terrains";

        private TerrainDatas terrainDatas;

        public TerrainLibrary() {
            terrainDatas = new TerrainDatas();
        }

        public TerrainElementData getTerrainElements(LocationType locationType, TerrainElementType elementType) {
            TerrainData[] terrains = null;
            foreach (TerrainData terrainData in terrainDatas.terrains) {
                if (terrainData.LocationType == locationType) {
                    foreach (TerrainElementData terrainElementData in terrainData.TerrainElementDatas) {
                        if (terrainElementData.Type == elementType) {
                            return terrainElementData;
                        }
                    }
                }
            }

            CustomConsole.WriteError("TerrainElementData with LocationType = " + locationType.ToString() + ", TerrainElementType = " + elementType.ToString() + " doesn't exist");
            return null;
        }

        public void parseDataFromXML(string uri) {
            CustomXmlSerializer xmlSerializer = new CustomXmlSerializer(uri, TerrainLibrary.TERRAINS_ROOT_XML_ELEMENT_NAME, terrainDatas);
            terrainDatas = (TerrainDatas)xmlSerializer.Deserialize();
            terrainDatas.DisplayToConsole();
        }
    }
}
