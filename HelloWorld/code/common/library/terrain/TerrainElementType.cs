﻿using System.Xml.Serialization;

namespace HelloWorld.code.common.library.terrain {
    public enum TerrainElementType {
        [XmlEnum(Name = "ground")]
        GROUND = 1,
        [XmlEnum(Name = "water")]
        WATER = 2,
        [XmlEnum(Name = "rock")]
        ROCK = 3,
        [XmlEnum(Name = "green")]
        GREEN = 4,
        [XmlEnum(Name = "road")]
        ROAD = 5,
        [XmlEnum(Name = "clouds")]
        CLOUDS = 6,
        [XmlEnum(Name = "sky")]
        SKY = 7,
        [XmlEnum(Name = "sun")]
        SUN = 8,
        [XmlEnum(Name = "rain")]
        RAIN = 9,
        [XmlEnum(Name = "snow")]
        SNOW = 10
    }
}
