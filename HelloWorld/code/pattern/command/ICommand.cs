﻿namespace HelloWorld.code.pattern.command {
    interface ICommand {
        void Execute();
    }
}
