﻿using HelloWorld.code.pattern.ommand;
using System.Collections.Generic;
using System.Linq;

namespace HelloWorld.code.pattern.command {
    class Invoker {
        private static List<Command> commandsQueue = new List<Command>();

        private IComparer<Command> commandPriorityComparer = null;
        public Invoker() {
            commandPriorityComparer = new CommandPriorityComparer();
        }

        public void AddCommandToQueue(Command command) {
            commandsQueue.Add(command);
            commandsQueue.Sort(commandPriorityComparer);
        }

        public void Run() {
            Command currentCommand = commandsQueue.First();
            if (currentCommand == null)
                return;

            commandsQueue.RemoveAt(0);
            currentCommand.Execute();
        }
    }
}
