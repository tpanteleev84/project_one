﻿using HelloWorld.code.pattern.ommand;
using System.Collections.Generic;

namespace HelloWorld.code.pattern.command {
    class CommandPriorityComparer : IComparer<Command> {
        public int Compare(Command a, Command b) {
            return a.Priority - b.Priority;
        }
    }
}
