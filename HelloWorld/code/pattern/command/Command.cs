﻿using HelloWorld.code.pattern.command;

namespace HelloWorld.code.pattern.ommand {
    class Command : ICommand {
        //assynch param
        private string type = null;
        public string Type {
            get {
                return type;
            }
            set {
                if (type == null)
                    type = value;
            }
        }
        private object data = null;
        public object Data {
            get {
                return data;
            }
            set {
                if (data == null)
                    data = value;
            }
        }
        private int priority = -1;
        public int Priority {
            get {
                return priority;
            }
            set {
                if (priority == -1)
                    priority = value;
            }
        }

        private void Clear() {
            data = null;
            type = null;
            priority = -1;
        }

        virtual public void Execute() {

        }
    }
}
