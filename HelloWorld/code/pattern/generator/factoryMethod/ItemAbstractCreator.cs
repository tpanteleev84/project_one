﻿namespace HelloWorld.code.pattern.generator.factoryMethod {
    abstract class ItemAbstractCreator {
        public abstract IItem CreateWeapon(string itemData);
        public abstract IItem CreateSupplies(string itemData);
    }
}
