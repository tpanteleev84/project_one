﻿namespace HelloWorld.code.pattern.generator.factoryMethod {
    interface IItem {
        IItem Clone(); //Prototype pattern. Use for copy stackable items.
    }
}
