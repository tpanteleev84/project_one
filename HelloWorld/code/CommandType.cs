﻿namespace HelloWorld.code {
    internal class CommandType {
        public const string REGISTER_NEW_USER = "register";
        public const string LOGIN = "login";
        public const string TEST = "t";
        public const string START_GAME = "start";
        public const string DESERIALIZE = "des";
    }
}
