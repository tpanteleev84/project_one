﻿using HelloWorld.code.common.module.model.unit;

namespace HelloWorld.contentserver.generator.mob {
    internal interface IUnit {
        string Name { get; }
        string Description { get; }
        UnitStatusType Status { get; }
    }
}
