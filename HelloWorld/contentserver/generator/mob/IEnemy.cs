﻿namespace HelloWorld.contentserver.generator.mob {
    internal interface IBattleUnit : IUnit {
        void DealDamage(int damage);
        void RestoreHP(int hp);
    }
}
