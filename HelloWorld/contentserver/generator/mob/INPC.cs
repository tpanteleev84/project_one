﻿using System;
using System.Collections.Generic;

namespace HelloWorld.contentserver.generator.mob {
    interface INPC : IUnit {
        List<String> GetQuests();
    }
}
