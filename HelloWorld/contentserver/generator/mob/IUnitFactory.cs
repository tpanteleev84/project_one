﻿namespace HelloWorld.contentserver.generator.mob {
    internal interface IUnitFactory //abstract factory
    {
        IBattleUnit CreateEnemyMob();
        INPC CreateNPCMob();
    }
}
