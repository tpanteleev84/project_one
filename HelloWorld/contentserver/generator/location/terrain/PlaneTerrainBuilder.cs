﻿using HelloWorld.code.common.library;
using HelloWorld.code.common.library.location;
using HelloWorld.code.common.library.terrain;
using HelloWorld.contentserver.generator.location.terrain;
using HelloWorld.code.util;
using System;

namespace HelloWorld.contentserver.generator.location.terrain {
    internal class PlaneTerrainBuilder : BaseTerrainBuilder {
        protected override LocationType LocationType => LocationType.PLANE;

        public override void CreateArea() {
            base.AddElementToTerrain(SceneData.Description);
            TerrainElementData terrainElementData = null;
            Random random = new Random();
            foreach (LocationTerrainElementData locationTerrainElementData in SceneData.LocationTerrainElementDatas) {
                terrainElementData = GameLibrary.Instance.Terrains.getTerrainElements(LocationType, locationTerrainElementData.Type);
                if (terrainElementData == null) {
                    CustomConsole.WriteWarning("TerrainElementData with LocationType=" + LocationType.ToString() + ", TerrainElementType=" + locationTerrainElementData.Type.ToString() + " doesn't exist");
                    continue;
                }

                base.AddElementToTerrain(terrainElementData.Descriptions[random.Next(terrainElementData.Descriptions.Count)]);
            }
        }

        public override void CreateNature() {

        }

        public override void CreateObjects() {

        }
    }
}
