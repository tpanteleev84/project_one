﻿using HelloWorld.code.common.library.location;

namespace HelloWorld.contentserver.generator.location.terrain {
    //pattern builder builder interface
    internal interface ITerrainBuilder {
        //TODO: data which description terrain
        void Init(SceneData sceneData);
        void Reset();
        void CreateArea();
        void CreateNature();
        void CreateObjects();
        string GetTerrain();
    }
}
