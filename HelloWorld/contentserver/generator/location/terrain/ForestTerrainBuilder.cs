﻿using HelloWorld.code.common.library.location;

namespace HelloWorld.contentserver.generator.location.terrain {
    internal class ForestTerrainBuilder : BaseTerrainBuilder {
        protected override LocationType LocationType => LocationType.FOREST;

        public override void CreateArea() {

        }
        public override void CreateNature() {

        }
        public override void CreateObjects() {

        }
    }
}
