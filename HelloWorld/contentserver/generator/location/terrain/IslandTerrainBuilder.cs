﻿using HelloWorld.code.common.library.location;

namespace HelloWorld.contentserver.generator.location.terrain {
    internal class IslandTerrainBuilder : BaseTerrainBuilder {
        protected override LocationType LocationType => LocationType.ISLAND;

        public override void CreateArea() {

        }
        public override void CreateNature() {

        }
        public override void CreateObjects() {

        }
    }
}
