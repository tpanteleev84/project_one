﻿using HelloWorld.code.common.library.location;
using HelloWorld.code.util;
using HelloWorld.contentserver.generator.location.terrain;
using System.Collections.Generic;

namespace HelloWorld.contentserver.generation.location.terrain {
    internal class TerrainBuilderDirector //pattern Builder director
    {
        private Dictionary<LocationType, ITerrainBuilder> terrainBuilderByLocation = null;

        public TerrainBuilderDirector() {
            terrainBuilderByLocation = new Dictionary<LocationType, ITerrainBuilder>();

            terrainBuilderByLocation.Add(LocationType.PLANE, new PlaneTerrainBuilder());
            terrainBuilderByLocation.Add(LocationType.FOREST, new PlaneTerrainBuilder());
            terrainBuilderByLocation.Add(LocationType.ISLAND, new PlaneTerrainBuilder());
            terrainBuilderByLocation.Add(LocationType.ROCK, new PlaneTerrainBuilder());
        }

        private ITerrainBuilder builder = null;
        public LocationType LocationType {
            set {
                builder = terrainBuilderByLocation[value];
            }
        }

        public string MakeTerrain(SceneData sceneData) {
            if (builder == null)
                CustomConsole.WriteError("TerrainBuilderDirector builder not setted");

            builder.Reset();
            builder.Init(sceneData);
            builder.CreateArea();
            builder.CreateNature();
            builder.CreateObjects();

            return builder.GetTerrain();
        }
    }
}
