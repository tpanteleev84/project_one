﻿using HelloWorld.code.common.library.location;

namespace HelloWorld.contentserver.generator.location.terrain {
    abstract class BaseTerrainBuilder : ITerrainBuilder {
        abstract protected LocationType LocationType { get; }

        protected SceneData SceneData = null;
        protected string Terrain = null;

        public void Init(SceneData sceneData) {
            this.SceneData = sceneData;
        }

        abstract public void CreateArea();
        abstract public void CreateNature();
        abstract public void CreateObjects();

        public string GetTerrain() {
            return Terrain;
        }

        public void Reset() {
            Terrain = null;
            SceneData = null;
        }

        protected void AddElementToTerrain(string terrainElement) {
            if (Terrain == null) {
                Terrain = "";
            }

            Terrain += " " + terrainElement;
        }
    }
}
