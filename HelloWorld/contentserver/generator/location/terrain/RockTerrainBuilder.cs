﻿using HelloWorld.code.common.library.location;

namespace HelloWorld.contentserver.generator.location.terrain {
    internal class RockTerrainBuilder : BaseTerrainBuilder {
        protected override LocationType LocationType => LocationType.ROCK;

        public override void CreateArea() {

        }
        public override void CreateNature() {

        }
        public override void CreateObjects() {

        }
    }
}
