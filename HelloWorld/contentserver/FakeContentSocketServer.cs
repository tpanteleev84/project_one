﻿using HelloWorld.code.common.library;
using HelloWorld.code.common.library.location;
using HelloWorld.code.common.module.core.transport.socket;
using HelloWorld.code.common.module.core.transport.socket.command;
using HelloWorld.code.common.module.model.location;
using HelloWorld.contentserver.generation.location.terrain;
using Newtonsoft.Json;
using System.Dynamic;

namespace HelloWorld.contentserver {
    class FakeContentSocketServer {
        private TerrainBuilderDirector terrainBuilder = null;
        private SocketClient socketClient = null;

        public FakeContentSocketServer(SocketClient socketClient) {
            terrainBuilder = new TerrainBuilderDirector();
            this.socketClient = socketClient;
        }

        public void ReceiveServerRequest(IServerCommand command) {
            dynamic requestObject = JsonConvert.DeserializeObject<ExpandoObject>(command.CommandString);
            dynamic commandParameters = requestObject.parameters;

            switch (command.CommandType) {
                case ServerCommandType.GET_LOCATION:
                    break;
                case ServerCommandType.GET_SCENE:
                    uint locationId = commandParameters.location_id;
                    uint sceneId = commandParameters.scene_id;

                    LocationData locationData = GameLibrary.Instance.Locations.GetLocationById(locationId);
                    SceneData sceneData = locationData.GetSceneById(sceneId);
                    SceneModel sceneModel = new SceneModel(sceneData);

                    terrainBuilder.LocationType = locationData.Type;

                    object parameters = new {
                        locationId = locationId,
                        sceneId = sceneId,
                        description = terrainBuilder.MakeTerrain(sceneData)
                    };

                    GetSceneServerCommand makeSceneCommand = new GetSceneServerCommand(parameters);
                    socketClient.SendServerRequest(makeSceneCommand);
                    break;
                default:
                    socketClient.SendServerRequest(command);
                    break;
            }
        }
    }
}
