﻿using HelloWorld.code.util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace HelloWorld.test
{
    /* Solutions
     * - incapsulate list into class
    */
    internal class TestUnitList: ITestSerializable
    {
        [XmlElement(ElementName = "unit")]
        public List<TestUnit> unitList = null;

        public TestUnitList()
        {
            unitList = new List<TestUnit>();
        }

        public void DisplayToConsole()
        {
            CustomConsole.WritePlainText("TestUnitList Count=" + unitList.Count);
            foreach (TestUnit testUnit in unitList)
            {
                testUnit.DisplayToConsole();
            }
        }
    }
}
