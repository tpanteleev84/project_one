﻿using HelloWorld.code.common.library;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace HelloWorld.test
{
    class XMLSerializeTester
    {
        private ITestSerializable serializableObjectInstance = null;
        private string rootElementName = "";
        private string uri = "";

        public XMLSerializeTester(string uri, string rootElementName, ITestSerializable serializableObjectInstance)
        {
            this.uri = uri;
            this.rootElementName = rootElementName;
            this.serializableObjectInstance = serializableObjectInstance;
        }

        public void Serialize()
        {
            XmlSerializer serializer = new XmlSerializer(serializableObjectInstance.GetType(), new XmlRootAttribute(rootElementName));
            using (XmlReader xmlReader = XmlReader.Create(uri))
            {
                ITestSerializable deserializableClasses = (ITestSerializable)serializer.Deserialize(xmlReader);
                deserializableClasses.DisplayToConsole();
            }
        }

        public void Deserialize()
        {

        }
    }
}
