﻿using HelloWorld.code.util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace HelloWorld.test
{
    [Serializable]
    public class TestUnit : ITestSerializable
    {
        [XmlAttribute(AttributeName = "id")]
        public uint Id { get; set; }
        [XmlAttribute(AttributeName = "type")]
        public string Type { get; set; }
        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }
        [XmlAttribute(AttributeName = "description")]
        public string Description { get; set; }

        public void DisplayToConsole()
        {
            CustomConsole.WritePlainText("TestUnit Id=" + Id + " Type=" + Type + " Name=" + Name + " Description=" + Description);
        }
    }
}
